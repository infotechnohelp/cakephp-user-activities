<?php
namespace UserActivities\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use UserActivities\Model\Table\UserRequestsTable;

/**
 * UserActivities\Model\Table\UserRequestsTable Test Case
 */
class UserRequestsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \UserActivities\Model\Table\UserRequestsTable
     */
    public $UserRequests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.UserActivities.UserRequests',
        'plugin.UserActivities.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserRequests') ? [] : ['className' => UserRequestsTable::class];
        $this->UserRequests = TableRegistry::getTableLocator()->get('UserRequests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserRequests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
