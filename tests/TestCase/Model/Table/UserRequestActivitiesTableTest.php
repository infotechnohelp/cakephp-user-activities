<?php
namespace UserActivities\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use UserActivities\Model\Table\UserRequestActivitiesTable;

/**
 * UserActivities\Model\Table\UserRequestActivitiesTable Test Case
 */
class UserRequestActivitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \UserActivities\Model\Table\UserRequestActivitiesTable
     */
    public $UserRequestActivities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.UserActivities.UserRequestActivities',
        'plugin.UserActivities.Requests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserRequestActivities') ? [] : ['className' => UserRequestActivitiesTable::class];
        $this->UserRequestActivities = TableRegistry::getTableLocator()->get('UserRequestActivities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserRequestActivities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
