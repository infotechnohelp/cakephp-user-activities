<?php
namespace UserActivities\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use UserActivities\Model\Table\UserVisitsTable;

/**
 * UserActivities\Model\Table\UserVisitsTable Test Case
 */
class UserVisitsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \UserActivities\Model\Table\UserVisitsTable
     */
    public $UserVisits;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.UserActivities.UserVisits',
        'plugin.UserActivities.Agents',
        'plugin.UserActivities.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserVisits') ? [] : ['className' => UserVisitsTable::class];
        $this->UserVisits = TableRegistry::getTableLocator()->get('UserVisits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserVisits);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
