<?php
namespace UserActivities\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use UserActivities\Model\Table\UserRequestActivityErrorsTable;

/**
 * UserActivities\Model\Table\UserRequestActivityErrorsTable Test Case
 */
class UserRequestActivityErrorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \UserActivities\Model\Table\UserRequestActivityErrorsTable
     */
    public $UserRequestActivityErrors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.UserActivities.UserRequestActivityErrors',
        'plugin.UserActivities.Requests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserRequestActivityErrors') ? [] : ['className' => UserRequestActivityErrorsTable::class];
        $this->UserRequestActivityErrors = TableRegistry::getTableLocator()->get('UserRequestActivityErrors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserRequestActivityErrors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
