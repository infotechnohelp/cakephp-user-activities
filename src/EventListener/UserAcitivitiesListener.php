<?php

namespace UserActivities\EventListener;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;
use Symfony\Component\Yaml\Yaml;
use UserActivities\Lib\Manager;

/**
 * Class UserAcitivitiesListener
 * @package UserActivities\EventListener
 */
class UserAcitivitiesListener implements EventListenerInterface
{

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return [
            'UserActivities.request' => 'writeRequestData',
            'UserActivities.externalRedirection' => 'externalRedirection',
        ];
    }

    public function externalRedirection(Event $event)
    {
        $UserActivitiesManager = new Manager();

        /** @var Controller $controller */
        $controller = $event->getSubject();

        $url = $controller->getRequest()->getQuery('url');

        $ExternalRedirectsTable = TableRegistry::getTableLocator()->get('UserActivities.UserExternalRedirects');

        $ExternalRedirectsTable->saveOrFail($ExternalRedirectsTable->newEntity([
            'visit_id' => $UserActivitiesManager->getCurrentVisitId($event),
            'url' => preg_replace("(^https?://)", "", $url),
        ]));

        $controller->redirect($url);
    }

    /**
     * @param Event $event
     */
    public function writeRequestData(Event $event)
    {
        /** @var \Cake\Controller\Controller $controller */
        $controller = $event->getSubject();

        $controller->set('requestId', null);

        /** @var \Cake\Network\Request $request */
        $request = $controller->getRequest();

        if (filter_var(getenv('INCOGNITO'), FILTER_VALIDATE_BOOLEAN) === true) {
            return;
        }


        $UserActivitiesManager = new Manager();

        $requestPath = $UserActivitiesManager->prepareRequestPath($request);

        $userActivitiesConfig = Yaml::parse(file_get_contents(CONFIG . 'user-activities.config.yml'));

        $query = $request->getQuery();

        if (array_key_exists('incognito', $query)) {
            if (filter_var($query['incognito'], FILTER_VALIDATE_BOOLEAN) === true) {
                if (array_key_exists('key', $query) && $query['key'] === $userActivitiesConfig['key']) {
                    $request->getSession()->write('INCOGNITO', true);
                }
            } else {
                $request->getSession()->write('INCOGNITO', false);
            }
        }

        if ($request->getSession()->read('INCOGNITO') === true) {
            return;
        }

        if (in_array($requestPath, $userActivitiesConfig['ignored-request-paths'], true)) {
            return;
        }

        foreach ($userActivitiesConfig['ignored-request-path-prefixes'] as $prefix) {
            if (strpos($requestPath, $prefix) !== false) {
                return;
            }
        }


        $currentVisitId = $UserActivitiesManager->getCurrentVisitId($event);

        $visitId = $currentVisitId ?? $UserActivitiesManager->createVisit($event)->get('id');

        $requestPayload = $UserActivitiesManager->prepareRequestPayload($request);

        list($preparedReferrer, $referrerPayload) = $UserActivitiesManager->prepareReferrerData($request);


        $UserRequestData = [
            'visit_id' => $visitId,
            'referrer' => ($request->referer() === '/') ? null : $preparedReferrer,
            'referrer_payload' => $referrerPayload,
            'request_method' => $request->getMethod(),
            'request_path' => $requestPath,
            'payload' => (empty($requestPayload)) ? null : json_encode($requestPayload),
        ];

        $UserRequestsTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequests');

        $result = $UserRequestsTable->saveOrFail($UserRequestsTable->newEntity($UserRequestData));

        $controller->set('requestId', $result->get('id'));
    }
}
