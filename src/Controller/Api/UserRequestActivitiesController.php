<?php

declare(strict_types=1);

namespace UserActivities\Controller\Api;

use UserActivities\Lib\Manager;

/**
 * Class UserRequestActivitiesController
 * @package AuthApi\Controller\Api
 */
class UserRequestActivitiesController extends \App\Controller\Api\AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow();
    }

    public function init()
    {
        $this->request->allowMethod('post');

        $requestData = $this->request->getData();

        $UserActivitiesManager = new Manager();

        $this->_setResponse(
            $UserActivitiesManager->createUserRequestActivity(
                (int)$requestData['request_id'],
                $requestData['event_log']
            )
        );
    }

    public function update()
    {
        $this->request->allowMethod('post');

        $requestData = $this->request->getData();

        $UserActivitiesManager = new Manager();

        $this->_setResponse(
            $UserActivitiesManager->updateUserRequestActivity(
                (int)$requestData['request_id'],
                $requestData['event_log']
            )
        );
    }

    public function latestEvent()
    {
        $this->request->allowMethod('post');

        $requestData = $this->request->getData();

        $UserActivitiesManager = new Manager();

        $this->_setResponse(
            $UserActivitiesManager->latestUserRequestActivity(
                (int)$requestData['request_id'],
                $requestData['timestamp']
            )
        );
    }

    public function error()
    {
        $this->request->allowMethod('post');

        $requestData = $this->request->getData();

        $UserActivitiesManager = new Manager();

        $this->_setResponse(
            $UserActivitiesManager->createUserRequestError(
                (int)$requestData['request_id'],
                $requestData['error_log'],
                $requestData['body_html']
            )
        );
    }
}
