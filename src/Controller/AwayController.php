<?php

declare(strict_types=1);

namespace UserActivities\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Event\EventManager;

class AwayController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function index()
    {
        $event = new Event('UserActivities.externalRedirection', $this);
        EventManager::instance()->dispatch($event);
    }
}
