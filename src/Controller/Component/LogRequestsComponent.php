<?php

namespace UserActivities\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\Event\EventManager;

class LogRequestsComponent extends Component
{
    public function beforeFilter()
    {
        $event = new Event('UserActivities.request', $this->getController());
        EventManager::instance()->dispatch($event);
    }
}
