<?php

namespace UserActivities\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;

/**
 * Class ReportsShell
 * @package UserActivities\Shell
 */
class ReportsShell extends Shell
{
    public function deviceTypes()
    {
        $UserRequestsTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequests');

        // @todo Get amount of minutes (5) from config
        $Query = $UserRequestsTable
            ->find();

        debug($Query->all()->first());
    }
}
