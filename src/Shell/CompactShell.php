<?php

namespace UserActivities\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;

/**
 * Class CompactShell
 * @package UserActivities\Shell
 */
class CompactShell extends Shell
{
    public function userRequests()
    {
        $UserRequestsTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequests');

        // @todo Get amount of minutes (5) from config
        $Query = $UserRequestsTable
            ->find()
            ->where(['TIMESTAMP(created) <=' => (new \DateTime('- 5 minutes'))->format('Y-m-d H:i:s')]);

        if (! $Query->all()->isEmpty()) {
            $fileName = time() . '_userRequests';
            $file     = new File(WWW_ROOT . 'compactedUserActivities' . DS . $fileName . '.json', true);

            $file->write(json_encode($Query->all()->toArray()));
            $this->out(sprintf('%s records compacted, file title: %s', $Query->count(), $fileName));

            foreach ($Query->all() as $UserRequest) {
                $UserRequestsTable->deleteOrFail($UserRequest);
            }
        }
    }
}
