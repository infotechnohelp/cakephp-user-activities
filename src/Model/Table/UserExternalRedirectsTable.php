<?php
namespace UserActivities\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserExternalRedirects Model
 *
 * @property \UserActivities\Model\Table\VisitsTable|\Cake\ORM\Association\BelongsTo $Visits
 *
 * @method \UserActivities\Model\Entity\UserExternalRedirect get($primaryKey, $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect newEntity($data = null, array $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect[] newEntities(array $data, array $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect[] patchEntities($entities, array $data, array $options = [])
 * @method \UserActivities\Model\Entity\UserExternalRedirect findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserExternalRedirectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_external_redirects');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserVisits', [
            'foreignKey' => 'visit_id',
            'className'  => 'UserActivities.UserVisits',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('url')
            ->requirePresence('url', 'create')
            ->allowEmptyString('url', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['visit_id'], 'UserVisits'));

        return $rules;
    }
}
