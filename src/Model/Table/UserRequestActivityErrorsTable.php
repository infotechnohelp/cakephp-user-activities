<?php
namespace UserActivities\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class UserRequestActivityErrorsTable
 * @package UserActivities\Model\Table
 */
class UserRequestActivityErrorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_request_activity_errors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserRequests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER',
            'className' => 'UserActivities.UserRequests'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('error_log')
            ->requirePresence('error_log', 'create')
            ->notEmpty('error_log');

        $validator
            ->scalar('body_html')
            ->requirePresence('body_html', 'create')
            ->notEmpty('body_html');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['request_id'], 'UserRequests'));

        return $rules;
    }
}
