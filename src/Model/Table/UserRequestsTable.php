<?php

namespace UserActivities\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class UserRequestsTable
 * @package UserActivities\Model\Table
 */
class UserRequestsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_requests');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className'  => 'AuthApi.Users',
        ]);

        $this->belongsTo('UserVisits', [
            'foreignKey' => 'visit_id',
            'className'  => 'UserActivities.UserVisits',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('referrer')
            ->allowEmpty('referrer');

        $validator
            ->scalar('referrer_payload')
            ->allowEmpty('referrer_payload');

        $validator
            ->scalar('request_path')
            ->allowEmpty('request_path');

        $validator
            ->scalar('payload')
            ->allowEmpty('payload');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     *
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['visit_id'], 'UserVisits'));

        return $rules;
    }
}
