<?php
namespace UserActivities\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class UserVisitsTable
 * @package UserActivities\Model\Table
 */
class UserVisitsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_visits');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserRequestAgents', [
            'foreignKey' => 'agent_id',
            'joinType' => 'INNER',
            'className'  => 'UserActivities.UserRequestAgents',
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className'  => 'AuthApi.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('ip')
            ->maxLength('ip', 191)
            ->requirePresence('ip', 'create')
            ->notEmpty('ip');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['agent_id'], 'UserRequestAgents'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
