<?php

namespace UserActivities\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Mobile_Detect;

/**
 * UserRequest Entity
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $referrer
 * @property string|null $referrer_payload
 * @property string|null $request_path
 * @property string|null $payload
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \AuthApi\Model\Entity\User $user
 */
class UserRequest extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_virtual = [
        'device_type',
    ];

    protected $_accessible = [
        'user_id' => true,
        'user' => true,
        'visit_id' => true,
        'user_visit' => true,
        'referrer' => true,
        'referrer_payload' => true,
        'request_method' => true,
        'request_path' => true,
        'payload' => true,
        'created' => true,
        'modified' => true,
    ];

    /**
     * @return int
     */
    public function _getDeviceType()
    {
        $detect = new Mobile_Detect();

        $AgentsTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequestAgents');

        $detect->setUserAgent($AgentsTable->get($this->get('agent_id'))->get('title'));

        if ($detect->isMobile()) {
            return 'Mobile';
        }

        if ($detect->isTablet()) {
            return 'Tablet';
        }

        // @todo Get is('Chrome') working on Desktop, same about iOS/Android, Chrome/Mozilla/IE/Opera

        return 'Desktop';
    }
    //@codingStandardsIgnoreEnd
}
