<?php
namespace UserActivities\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class UserVisit
 * @package UserActivities\Model\Entity
 */
class UserVisit extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        'ip' => true,
        'agent_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'user_request_agent' => true,
        'user' => true
    ];
    //@codingStandardsIgnoreEnd
}
