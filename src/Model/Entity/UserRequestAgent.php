<?php
namespace UserActivities\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class UserRequest
 * @package UserActivities\Model\Entity
 */
class UserRequestAgent extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        'title' => true,
        'created' => true,
        'modified' => true,
    ];
    //@codingStandardsIgnoreEnd
}
