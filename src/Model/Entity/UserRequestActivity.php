<?php

namespace UserActivities\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserRequestActivity Entity
 *
 * @property int $id
 * @property int $request_id
 * @property string $event_log
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \UserActivities\Model\Entity\Request $request
 */
class UserRequestActivity extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        'request_id' => true,
        'event_log' => true,
        'latest_event' => true,
        'created' => true,
        'modified' => true,
        'request' => true
    ];
    //@codingStandardsIgnoreEnd
}
