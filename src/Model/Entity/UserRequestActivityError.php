<?php
namespace UserActivities\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserRequestActivityError Entity
 *
 * @property int $id
 * @property int $request_id
 * @property string $error_log
 * @property string $body_html
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \UserActivities\Model\Entity\Request $request
 */
class UserRequestActivityError extends Entity
{
    //@codingStandardsIgnoreStart
    protected $_accessible = [
        'request_id' => true,
        'error_log' => true,
        'body_html' => true,
        'created' => true,
        'modified' => true,
        'request' => true
    ];
    //@codingStandardsIgnoreEnd
}
