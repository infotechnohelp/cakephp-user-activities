<?php
namespace UserActivities\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserExternalRedirect Entity
 *
 * @property int $id
 * @property int $visit_id
 * @property string $url
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \UserActivities\Model\Entity\Visit $visit
 */
class UserExternalRedirect extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'visit_id' => true,
        'url' => true,
        'created' => true,
        'modified' => true,
        'visit' => true
    ];
}
