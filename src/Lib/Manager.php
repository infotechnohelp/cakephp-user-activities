<?php

namespace UserActivities\Lib;

use Cake\Event\Event;
use Cake\Network\Request;
use Cake\ORM\ResultSet;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use UserActivities\Model\Entity\UserRequestActivity;
use UserActivities\Model\Entity\UserVisit;
use UserActivities\Model\Table\UserRequestActivitiesTable;
use UserActivities\Model\Table\UserRequestActivityErrorsTable;
use UserActivities\Model\Table\UserVisitsTable;

/**
 * Class Manager
 * @package UserActivities\Lib
 */
class Manager
{
    /**
     * @param string $requestAgentTitle
     *
     * @return mixed|null
     */
    public function getRequestAgentId(string $requestAgentTitle)
    {
        /** @var \UserActivities\Model\Table\UserRequestAgentsTable $RequestAgentsTable */
        $RequestAgentsTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequestAgents');

        /** @var \UserActivities\Model\Entity\UserRequestAgent $RequestAgent */
        $RequestAgent = $RequestAgentsTable->findByTitle($requestAgentTitle)->first();

        return (empty($RequestAgent)) ? null : $RequestAgent->get('id');
    }

    /**
     * @param Event $event
     * @param int $visitLimitInMinutes
     * @return null
     */
    public function getCurrentVisitId(Event $event, int $visitLimitInMinutes = 5)
    {
        /** @var \Cake\Controller\Controller $controller */
        $controller = $event->getSubject();
        /** @var \Cake\Network\Request $request */
        $request = $controller->getRequest();

        if (empty($this->getRequestAgentId($request->getEnv('HTTP_USER_AGENT')))) {
            return null;
        }

        $ip = $request->clientIp();
        $modify = sprintf('- %s min', $visitLimitInMinutes);
        $whereConditions = [
            'ip' => $ip,
            'TIMESTAMP (created) >=' => (new \DateTime())->modify($modify)->format('Y-m-d H:i:s'),
        ];

        $userId = $controller->Auth->user()['id'];
        if (empty($userId)) {
            $whereConditions[] = 'user_id IS NULL';
        } else {
            $whereConditions['user_id'] = $userId;
        }

        /** @var UserVisitsTable $UserVisitsTable */
        $UserVisitsTable = TableRegistry::getTableLocator()->get('UserActivities.UserVisits');

        /** @var ResultSet $result */
        $result = $UserVisitsTable->find()->where($whereConditions)->all();

        return ($result->isEmpty()) ? null : $result->first()->get('id');
    }

    /**
     * @param Event $event
     * @return mixed
     */
    public function createVisit(Event $event)
    {
        /** @var \Cake\Controller\Controller $controller */
        $controller = $event->getSubject();

        /** @var \Cake\Network\Request $request */
        $request = $controller->getRequest();

        $UserVisitData = [
            'ip' => $request->clientIp(),
            'user_id' => $controller->Auth->user()['id'],
        ];

        $requestAgentTitle = $request->getEnv('HTTP_USER_AGENT');

        $associated = [];

        if (empty($this->getRequestAgentId($requestAgentTitle))) {
            $UserVisitData = array_merge($UserVisitData, [
                'user_request_agent' => [
                    'title' => $requestAgentTitle,
                ],
            ]);

            $associated[] = 'UserRequestAgents';
        } else {
            $UserVisitData = array_merge($UserVisitData, [
                'agent_id' => $this->getRequestAgentId($requestAgentTitle),
            ]);
        }

        /** @var UserVisitsTable $UserVisitsTable */
        $UserVisitsTable = TableRegistry::getTableLocator()->get('UserActivities.UserVisits');

        /** @var UserVisit $UserVisit */
        $UserVisit = $UserVisitsTable->saveOrFail(
            $UserVisitsTable->newEntity($UserVisitData),
            ['associated' => $associated]
        );

        return $UserVisit;
    }

    /**
     * @param Request $request
     * @return mixed|string
     */
    public function prepareRequestPath(Request $request)
    {
        $requestPath = $request->getRequestTarget();

        if (strpos($requestPath, '?') !== false) {
            $requestPath = explode('?', $requestPath)[0];
        }
        $requestPath = preg_replace('/^\//', '', $requestPath);

        return empty($requestPath) ? '/' : $requestPath;
    }

    /**
     * @param Request $request
     * @return array|null
     */
    public function prepareRequestPayload(Request $request)
    {

        $requestPayload = [
            $request->getQuery(),
            $request->getData(),
        ];

        if (array_key_exists('password', $requestPayload[0])) {
            $requestPayload[0]['password'] = '***';
        }

        if (array_key_exists('password', $requestPayload[1])) {
            $requestPayload[1]['password'] = '***';
        }

        if (empty($request->getQuery()) && empty($request->getData())) {
            $requestPayload = null;
        }

        return $requestPayload;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function prepareReferrerData(Request $request)
    {
        $referrer = $request->referer();

        $referrer = (strpos($referrer, Router::url('/', true)) !== false) ?
            str_replace(rtrim(Router::url('/', true), '/'), '', $referrer) :
            $referrer;

        if (strpos($referrer, '?') !== false) {
            $cleanReferrer = explode('?', $referrer)[0];
            $referrerPayload = str_replace($cleanReferrer . '?', '', $referrer);
        } else {
            $cleanReferrer = $referrer;
            $referrerPayload = null;
        }

        return [$cleanReferrer, $referrerPayload];
    }

    /**
     * @param \DateTimeImmutable|null $from
     * @param \DateTimeImmutable|null $to
     */
    // @todo
    public function filterVisits(\DateTimeImmutable $from = null, \DateTimeImmutable $to = null)
    {
    }

    /**
     * @param int $requestId
     * @return mixed
     */
    public function findUserRequestActivity(int $requestId)
    {
        $UserRequestActivitiesTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequestActivities');

        return $UserRequestActivitiesTable->findByRequestId($requestId)->first();
    }

    /**
     * @param int $requestId
     * @param string $eventLog
     * @return \Cake\Datasource\EntityInterface
     */
    public function createUserRequestActivity(int $requestId, string $eventLog)
    {
        $UserRequestActivitiesTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequestActivities');

        $UserRequestActivity = $UserRequestActivitiesTable->newEntity([
            'request_id' => $requestId,
            'event_log' => $eventLog,
        ]);

        $UserRequestActivitiesTable->saveOrFail($UserRequestActivity);

        return $UserRequestActivity;
    }

    /**
     * @param int $requestId
     * @param string $eventLogJson
     * @return \Cake\Datasource\EntityInterface
     */
    public function updateUserRequestActivity(int $requestId, string $eventLogJson)
    {
        /** @var UserRequestActivity $UserRequestActivity */
        $UserRequestActivity = $this->findUserRequestActivity($requestId);

        $eventLog = json_decode($UserRequestActivity->get('event_log'));

        $eventLog[] = json_decode($eventLogJson);

        $UserRequestActivity->set('event_log', json_encode($eventLog));

        /** @var UserRequestActivitiesTable $UserRequestActivitiesTable */
        $UserRequestActivitiesTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequestActivities');

        return $UserRequestActivitiesTable->saveOrFail($UserRequestActivity);
    }

    /**
     * @param int $requestId
     * @param string $timestamp
     * @return \Cake\Datasource\EntityInterface
     */
    public function latestUserRequestActivity(int $requestId, string $timestamp)
    {
        /** @var UserRequestActivity $UserRequestActivity */
        $UserRequestActivity = $this->findUserRequestActivity($requestId);


        $UserRequestActivity->set('latest_event', $timestamp);

        /** @var UserRequestActivitiesTable $UserRequestActivitiesTable */
        $UserRequestActivitiesTable = TableRegistry::getTableLocator()->get('UserActivities.UserRequestActivities');

        return $UserRequestActivitiesTable->saveOrFail($UserRequestActivity);
    }

    private function userRequestErrorExists(int $requestId, string $errorLog)
    {
        /** @var UserRequestActivityErrorsTable $UserRequestActivityErrorsTable */
        $UserRequestActivityErrorsTable = TableRegistry::getTableLocator()
            ->get('UserActivities.UserRequestActivityErrors');

        $result = $UserRequestActivityErrorsTable->find()->where([
            'request_id' => $requestId,
            'error_log' => $errorLog,
        ])->first();

        return (empty($result)) ? false : true;
    }

    /**
     * @param int $requestId
     * @param string $errorLog
     * @param string $bodyHtml
     * @return \Cake\Datasource\EntityInterface
     */
    public function createUserRequestError(int $requestId, string $errorLog, string $bodyHtml)
    {
        /** @var UserRequestActivityErrorsTable $UserRequestActivityErrorsTable */
        $UserRequestActivityErrorsTable = TableRegistry::getTableLocator()
            ->get('UserActivities.UserRequestActivityErrors');

        if ($this->userRequestErrorExists($requestId, $errorLog)) {
            return $UserRequestActivityErrorsTable->find()->where([
                'request_id' => $requestId,
                'error_log' => $errorLog,
            ])->first();
        }

        return $UserRequestActivityErrorsTable->saveOrFail($UserRequestActivityErrorsTable->newEntity([
            'request_id' => $requestId,
            'error_log' => $errorLog,
            'body_html' => gzcompress($bodyHtml, 9),
        ]));
    }
}
