<?php
use Migrations\AbstractMigration;

class CreateUserRequests extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_requests');
        $table->addColumn('visit_id', 'integer', [
            'default' => null,
            'limit'   => 11,
            'null'    => false,
        ]);
        $table->addColumn('referrer', 'text', [
            'default' => null,
            'null'    => true,
        ]);
        $table->addColumn('referrer_payload', 'text', [
            'default' => null,
            'null'    => true,
        ]);
        $table->addColumn('request_method', 'string', [
            'default' => null,
            'limit'   => 10,
            'null'    => false,
        ]);
        $table->addColumn('request_path', 'text', [
            'default' => null,
            'null'    => true,
        ]);
        $table->addColumn('payload', 'text', [
            'default' => null,
            'null'    => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null'    => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null'    => false,
        ]);
        $table->addIndex('created');
        $table->addIndex('modified');
        $table->create();
    }
}
